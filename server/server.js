var port = process.env.PORT || 3000,
    env = process.env.NODE_ENV || 'development';

var express = require('express'),
    server = express(),
    router = require('./router'),
    bodyParser = require('body-parser'),
    logger = require('morgan'),
    fs = require('fs'),
    utils = require('./utils');

var accessLogStream = fs.createWriteStream(__dirname + 
        '/access.log', { flags: 'a' }),
    devFormat = ':method :url :status :response-time ms - :res[content-length]';

if ('development' === env) {
    server.use(logger('dev'));
    server.use(logger('[:date[clf]] ' + devFormat, {stream: accessLogStream}));
} else {
    server.use(logger('common', { stream: accessLogStream }));
}

server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.use('/api/coordinates', [utils.auth, router]);
server.use((req, res) => {
    res.status(400).json({ error: 'bad request' });
});

server.listen(port, () => {
  console.log('Server listening on port: ' + port);
});